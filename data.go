package main

import (
	"encoding/json"

	"gitlab.com/psyhomb/gcl/cmd"
	"gitlab.com/psyhomb/gcl/kv"
)

// LockData holds lock related data that will be stored as value in the key/value store
type LockData struct {
	Git        GitData `json:"git"`
	CI         CIData  `json:"ci"`
	LockReason string  `json:"lock_reason"`
	LockStatus string  `json:"lock_status"`
}

// GitData holds git related data
type GitData struct {
	UserID    string `json:"user_id"`
	UserEmail string `json:"user_email"`
}

// CIData holds GitLab related data (CI/CD pipeline environment)
type CIData struct {
	EnvironmentName string `json:"environment_name"`
	ProjectName     string `json:"project_name"`
	ProjectID       string `json:"project_id"`
	ProjectURL      string `json:"project_url"`
	PipelineID      string `json:"pipeline_id"`
	JobStage        string `json:"job_stage"`
	CommitShortSHA  string `json:"commit_short_sha"`
	CommitRefName   string `json:"commit_ref_name"`
	CommitTag       string `json:"commit_tag"`
}

// Return stored lock data
func showLock(k string) ([]byte, error) {
	d, err := kv.GetKV(k)
	if err != nil {
		return nil, err
	}

	return d, nil
}

// Release lock
func relLock(k string) error {
	d := `{
    "lock_status": "unsealed"
}`
	err := kv.PutKV(k, d)
	if err != nil {
		return err
	}

	return nil
}

// Check if key exist
func isKeyExist(k string) (bool, error) {
	d, err := kv.GetKV(k)
	if err != nil {
		return false, err
	}

	if d == nil {
		return false, nil
	}

	return true, nil
}

// Check if lock released
func (ld LockData) isLockReleased(k string) (bool, error) {
	var r bool

	d, err := kv.GetKV(k)
	if err != nil {
		return r, err
	}

	err = ld.unmarshaler(d)
	if err != nil {
		return r, err
	}

	if ld.LockStatus == "unsealed" {
		r = true
	}

	return r, nil
}

// Acquire lock
func (ld *LockData) acqLock(k string) error {
	ld.Git.UserID = userID
	ld.Git.UserEmail = cmd.FlagUserEmail
	ld.CI.EnvironmentName = cmd.FlagEnvName
	ld.CI.ProjectName = cmd.FlagServiceName
	ld.CI.ProjectID = projectID
	ld.CI.ProjectURL = projectURL
	ld.CI.PipelineID = pipelineID
	ld.CI.JobStage = jobStage
	ld.CI.CommitShortSHA = commitShortSHA
	ld.CI.CommitRefName = commitRefName
	ld.CI.CommitTag = commitTag
	ld.LockReason = cmd.FlagLockReason
	ld.LockStatus = "sealed"

	d, err := ld.marshaler()
	if err != nil {
		return err
	}

	err = kv.PutKV(k, string(d))
	if err != nil {
		return err
	}

	return nil
}

// Unmarshal JSON to struct
func (ld *LockData) unmarshaler(d []byte) error {
	err := json.Unmarshal(d, ld)
	if err != nil {
		return err
	}

	return nil
}

// Marshal struct to JSON
func (ld *LockData) marshaler() ([]byte, error) {
	d, err := json.MarshalIndent(ld, "", "    ")
	if err != nil {
		return nil, err
	}

	return d, nil
}
