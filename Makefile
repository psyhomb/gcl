BIN_NAME = gcl
REVISION ?= $(shell git rev-parse --short HEAD)
GO_VERSION ?= 1.13

.PHONY: build
build:
	go get -d
	CGO_ENABLED=0 go build -ldflags="-s -w -X gitlab.com/psyhomb/gcl/cmd.binName=${BIN_NAME} -X gitlab.com/psyhomb/gcl/cmd.binVersion=${REVISION}" -o ${BIN_NAME}

.PHONY: build_linux
build_linux:
	docker run --rm -v $(shell pwd):/usr/src/${BIN_NAME} -w /usr/src/${BIN_NAME} golang:${GO_VERSION} make

.PHONY: help
help:
	@echo "build          - Compile go code and provide binary for ${GOOS} OS"
	@echo "build_linux    - Compile go code inside of docker container and provide binary for linux OS"
