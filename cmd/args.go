package cmd

import (
	"fmt"
	"os"

	flag "github.com/spf13/pflag"
)

// Program name and version
var (
	binName    string
	binVersion string
)

// Stored command line arguments values
var (
	FlagLock           bool
	FlagUnlock         bool
	FlagShow           bool
	FlagHelp           bool
	FlagVersion        bool
	FlagUserEmail      string
	FlagServiceName    string
	FlagEnvName        string
	FlagLockReason     string
	FlagConsulKeyPath  string
	FlagConsulHTTPAddr string
)

// Initialize command line arguments
func init() {
	flag.BoolVarP(
		&FlagLock,
		"lock", "l",
		false,
		"Acquire lock (required)")

	flag.BoolVarP(
		&FlagUnlock,
		"unlock", "u",
		false,
		"Release lock (required)")

	flag.BoolVarP(
		&FlagShow,
		"show", "S",
		false,
		"Show lock status (required)")

	flag.StringVarP(
		&FlagUserEmail,
		"user-email", "U",
		"",
		"User's email address (overrides GITLAB_USER_EMAIL env variable)")

	flag.StringVarP(
		&FlagServiceName,
		"service-name", "s",
		"",
		"Service name (overrides CI_PROJECT_NAME env variable)")

	flag.StringVarP(
		&FlagEnvName,
		"env-name", "e",
		"",
		"Environment name (overrides CI_ENVIRONMENT_NAME env variable)")

	flag.StringVarP(
		&FlagLockReason,
		"lock-reason", "R",
		"",
		"Lock reason (overrides LOCK_REASON env variable)")

	flag.StringVarP(
		&FlagConsulKeyPath,
		"consul-key-path", "k",
		"gitlab/cd/projects",
		"Consul KV key path")

	flag.StringVarP(
		&FlagConsulHTTPAddr,
		"consul-http-addr", "C",
		"",
		"Consul HTTP API address (overrides CONSUL_HTTP_ADDR env variable)")

	flag.BoolVarP(
		&FlagHelp,
		"help", "h",
		false,
		"Print this help")

	flag.BoolVarP(
		&FlagVersion,
		"version", "v",
		false,
		"Print program version")
}

// Parse command line arguments
func Parse() {
	flag.Parse()

	// Print help message
	if FlagHelp {
		PrintHelp()
		os.Exit(1)
	}

	// Print program version
	if FlagVersion {
		PrintVersion()
		os.Exit(0)
	}

	// Required options
	if !FlagLock && !FlagUnlock && !FlagShow {
		fmt.Println("[ERROR] At least one of these required options must be used: --lock, --unlock or --show")
		PrintHelp()
		os.Exit(1)
	}
}

// PrintHelp prints usage help
func PrintHelp() {
	fmt.Fprintf(os.Stderr, "Usage: %s OPTIONS\n\nOPTIONS:\n", binName)
	flag.PrintDefaults()
}

// PrintVersion prints program version
func PrintVersion() {
	fmt.Fprintf(os.Stdout, "v%s\n", binVersion)
}
