module gitlab.com/psyhomb/gcl

go 1.13

require (
	github.com/hashicorp/consul/api v1.3.0
	github.com/spf13/pflag v1.0.5
)
