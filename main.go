package main

import (
	"fmt"
	"os"

	"gitlab.com/psyhomb/gcl/cmd"
)

// Environment variables
var (
	userID         string
	projectID      string
	projectURL     string
	pipelineID     string
	jobStage       string
	commitShortSHA string
	commitRefName  string
	commitTag      string
)

// Lock data collection
var ld LockData

func init() {
	cmd.Parse()

	// Collect data from environment variables if cmdline flag isn't specified (flag have precedence over env)
	if len(cmd.FlagUserEmail) == 0 {
		cmd.FlagUserEmail, _ = os.LookupEnv("GITLAB_USER_EMAIL")
	}

	if len(cmd.FlagServiceName) == 0 {
		cmd.FlagServiceName, _ = os.LookupEnv("CI_PROJECT_NAME")
	}

	if len(cmd.FlagEnvName) == 0 {
		cmd.FlagEnvName, _ = os.LookupEnv("CI_ENVIRONMENT_NAME")
	}

	if len(cmd.FlagLockReason) == 0 {
		cmd.FlagLockReason, _ = os.LookupEnv("LOCK_REASON")
	}

	if len(cmd.FlagConsulHTTPAddr) == 0 {
		v, ok := os.LookupEnv("CONSUL_HTTP_ADDR")
		if !ok {
			cmd.FlagConsulHTTPAddr = "http://127.0.0.1:8500"
		} else {
			cmd.FlagConsulHTTPAddr = v
		}
	}

	// Collect data from environment variables
	userID, _ = os.LookupEnv("GITLAB_USER_ID")
	projectID, _ = os.LookupEnv("CI_PROJECT_ID")
	projectURL, _ = os.LookupEnv("CI_PROJECT_URL")
	pipelineID, _ = os.LookupEnv("CI_PIPELINE_ID")
	jobStage, _ = os.LookupEnv("CI_JOB_STAGE")
	commitShortSHA, _ = os.LookupEnv("CI_COMMIT_SHORT_SHA")
	commitRefName, _ = os.LookupEnv("CI_COMMIT_REF_NAME")
	commitTag, _ = os.LookupEnv("CI_COMMIT_TAG")
}

func main() {
	// Key name where lock data will be stored on K/V store
	kn := "lock"

	kp := cmd.FlagConsulKeyPath
	sn := cmd.FlagServiceName
	en := cmd.FlagEnvName

	switch {
	case cmd.FlagLock:
		ld.lock(kp, sn, en, kn)
	case cmd.FlagUnlock:
		ld.unlock(kp, sn, en, kn)
	case cmd.FlagShow:
		ld.show(kp, sn, en, kn)
	}
}

// Acquire lock by storing lock data
func (ld *LockData) lock(keyPath, serviceName, envName, keyName string) {
	if len(serviceName) == 0 || len(envName) == 0 {
		fmt.Println("[ERROR] --service-name and --env-name are mandatory when using --lock option")
		cmd.PrintHelp()
		os.Exit(3)
	}

	k := fmt.Sprintf("%s/%s/%s/%s", keyPath, serviceName, envName, keyName)
	keyExist, err := isKeyExist(k)
	if err != nil {
		fmt.Printf("[ERROR] %v\n", err)
		os.Exit(3)
	}
	if !keyExist {
		// It will create a new key with default value
		relLock(k)
	}

	lockReleased, err := ld.isLockReleased(k)
	if err != nil {
		fmt.Printf("[ERROR] %v\n", err)
		os.Exit(3)
	}
	if !lockReleased {
		fmt.Printf("[WARNING] Acquiring lock failed: %s/%s\n", serviceName, envName)
		os.Exit(3)
	}

	err = ld.acqLock(k)
	if err != nil {
		fmt.Printf("[ERROR] %v\n", err)
		os.Exit(3)
	}

	fmt.Printf("[INFO] Lock successfully acquired: %s/%s\n", serviceName, envName)
}

// Release lock by storing default data as value
func (ld *LockData) unlock(keyPath, serviceName, envName, keyName string) {
	k := fmt.Sprintf("%s/%s/%s/%s", keyPath, serviceName, envName, keyName)
	err := relLock(k)
	if err != nil {
		fmt.Printf("[ERROR] Releasing lock for %s/%s failed with error: %v\n", serviceName, envName, err)
		os.Exit(3)
	}

	fmt.Printf("[INFO] Lock successfully released: %s/%s\n", serviceName, envName)
}

// Show (print) lock data
func (ld *LockData) show(keyPath, serviceName, envName, keyName string) {
	k := fmt.Sprintf("%s/%s/%s/%s", keyPath, serviceName, envName, keyName)
	keyExist, err := isKeyExist(k)
	if err != nil {
		fmt.Printf("[ERROR] %v\n", err)
		os.Exit(3)
	}
	if !keyExist {
		fmt.Printf("[INFO] Key %s does not exist, run --lock or --unlock to create it\n", k)
		os.Exit(0)
	}

	d, err := showLock(k)
	if err != nil {
		fmt.Printf("[ERROR] Failed to show lock data for %s/%s with error: %v\n", serviceName, envName, err)
		os.Exit(3)
	}

	fmt.Printf("%s\n", string(d))
}
