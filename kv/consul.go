package kv

import (
	"fmt"
	"os"

	"github.com/hashicorp/consul/api"
)

// KV client
var kvc *api.KV

func init() {
	c, err := api.NewClient(api.DefaultConfig())
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	kvc = c.KV()
}

// PutKV creates key/value pair
func PutKV(k, v string) error {
	p := &api.KVPair{
		Key:   k,
		Value: []byte(v),
	}

	_, err := kvc.Put(p, nil)
	if err != nil {
		return err
	}

	return nil
}

// GetKV fetches value for the specified key,
func GetKV(k string) ([]byte, error) {
	p, _, err := kvc.Get(k, nil)
	if err != nil {
		return nil, err
	}

	if p == nil {
		return nil, nil
	}

	return p.Value, nil
}
