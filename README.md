gcl
===

GitLab CI/CD Consul Lock tool

Build
-----

Compile go code and provide binary for `${GOOS}` OS

```bash
make build
```

Compile go code inside of docker container and provide binary for linux OS

```bash
make build_linux
```

Usage
-----

Acquire lock

```plain
./gcl --lock --service-name service1 --env-name test
```

Show lock data

```plain
./gcl --show --service-name service1 --env-name test
```

Output:

```json
{
    "git": {
        "user_id": "",
        "user_email": ""
    },
    "ci": {
        "environment_name": "test",
        "project_name": "service1",
        "project_id": "",
        "project_url": "",
        "pipeline_id": "",
        "job_stage": "",
        "commit_short_sha": "",
        "commit_ref_name": "",
        "commit_tag": ""
    },
    "lock_reason": "",
    "lock_status": "sealed"
}
```

**Note:** Empty keys will be populated if program executed in GitLab's pipeline environment

Release lock

```plain
./gcl --unlock --service-name service1 --env-name test
```
